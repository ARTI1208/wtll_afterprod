import axios from 'axios'

export const SERVER = axios.create({
    // baseURL: `http://192.168.1.71:9666/api`,
    baseURL: `https://wtllbackend.cfapps.io/api`,
    headers: {
        'Access-Control-Allow-Origin': 'https://wtllbackend.cfapps.io',
        'Access-Control-Allow-Methods': 'GET,POST,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Credentials': true
    }
});