import Vue from 'vue';
import Vuex from 'vuex';
import {SERVER} from "../components/server-coordinator";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        author: undefined,
    },
    getters: {
        id: (state) => {
            return state.author ? state.author.id : undefined
        },
        avatarUrl: (state) => {
            return state.author ? SERVER.defaults.baseURL + "/users/authors/avatar?id=" + state.author.id : undefined
        }
    },
    mutations: {
        setAuthor(state, newAuthor) {
            state.author = newAuthor
        }
    },
    actions: {
    },
    modules: {
    },
})
