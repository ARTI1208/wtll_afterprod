import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {

        title: "",
        preview: null,

        blocks: [{type: "text", data: "", id: 0, style: "simple"}],

        articleTypeIndex: 0,
        tags: [],
        author: null,
        illustrator: null,
        isMain: false,
    },
    getters: {
        articleTypes() {
            return ["review", "longread", "podcast", "compilation"];
        },
        textBlockTypes() {
            return ["simple", "title", "subtitle", "lead", "sidebar"];
        },
    },
    mutations: {
        setTitle(state, newTitle) {
            state.title = newTitle;
        },
        onTextBlockDataChanged(state, elem) {
            state.blocks[elem.index].data = elem.data;
        },
        onTextBlockStyleChanged(state, elem) {
            state.blocks[elem.index].style = elem.style;
        },
        deleteBlock(state, index) {
            state.blocks.splice(index, 1);
        },
        setPreview(state, newPreview) {
            state.preview = newPreview;
        },
        setArticleType(state, newType) {
            state.articleTypeIndex = newType;
        },
        setMain(state, isMain) {
            state.isMain = isMain;
        },
        setTags(state, value) {
            Object.assign(state.tags, value);
        },
        setComponents(state, value) {
            Object.assign(state.blocks, value);
        },
    },
    actions: {
    },
    modules: {
    }
})
