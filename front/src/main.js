import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from "./App";
import LoginPage from "./components/login/LoginPage";
import ArticleWritingPage from "./components/article/writing/ArticleWritingPage";
import TypedTab from "./components/main/TypedTab";
import {SERVER} from "./components/server-coordinator";
import SearchPage from "./components/search/SearchPage";
import ArticleReadingPage from "./components/article/reading/ArticleReadingPage";
import AboutUsPage from "./components/about/AboutUsPage";
import AuthorPage from "./components/authorpage/AuthorPage";
import MyPage from "./components/authorpage/MyPage";
import RegisterPage from "./components/register/RegisterPage";

import AuthorStore from "./store/user_data_store";

const routes = [
    {
        path: '/',
        redirect: '/articles/review'
    },
    {
        path: '/author/:id',
        name: 'AuthorPage',
        component: AuthorPage
    },
    {
        path: '/settings',
        name: 'MyPage',
        component: MyPage
    },
    {
        path:'/register',
        name:'registerPage',
        component:RegisterPage
    },
    {
        path: '/login',
        name: 'loginPage',
        component: LoginPage
    },
    {
        path: '/write/:code?',
        name: 'writer',
        component: ArticleWritingPage
    },
    {
        path: '/search',
        name: 'search',
        component: SearchPage,

    },
    {
        path: '/about',
        name: 'about',
        component: AboutUsPage,

    },
    {
        path: '/articles/:type',
        name: 'articles',
        component: TypedTab,

    },
    {
        path: '/read/:code',
        name: 'read',
        component: ArticleReadingPage,

    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
});

// eslint-disable-next-line no-unused-vars
router.afterEach((to, from) => {
    window.scrollTo(0, 0);
});

// eslint-disable-next-line no-unused-vars
router.beforeEach((to, from, next) => {
    SERVER
        .get("/users/authors/me", {
            withCredentials: true
        })
        // eslint-disable-next-line no-unused-vars
        .then(response => {
            AuthorStore.commit("setAuthor", response.data)
            next()
        })
        // eslint-disable-next-line no-unused-vars
        .catch(e => {
            AuthorStore.commit("setAuthor", undefined)
            next()
        })
})

export function getArticleUrl(articleCode) {
    return "/read/" + articleCode
}

export function getBlobUri(code, articleCode) {
    const result = SERVER.defaults.baseURL + "/article/file?article=" + articleCode + "&code=" + code;
    console.log(result);
    return result
}

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
