package ru.wtll.back.users;

public class SocialNetworkLink {
    public SocialNetworkLink(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public SocialNetworkLink() {
    }

    public String title;
    public String link;
}
