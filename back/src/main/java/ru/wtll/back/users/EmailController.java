package ru.wtll.back.users;

import com.sun.mail.smtp.SMTPTransport;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.io.File;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

@Component
@RestController
@RequestMapping("api/email")
public class EmailController {

    private final String username;
    private final String password;
    private final Properties props;

    @Autowired
    PostgresDAO dbApi;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    void setJdb(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sqlCodes = "CREATE TABLE IF NOT EXISTS codes(code text)";
        jdbcTemplate.execute(sqlCodes);

    }

    private String generateAndSaveCode() {
        String code = UUID.randomUUID().toString();
        String sqlInsert = "INSERT INTO codes(code) VALUES(?)";
        jdbcTemplate.update(sqlInsert, code);
        return code;
    }

    public EmailController() {
        this.username = System.getenv("EMAIL");
        this.password = System.getenv("PASSWORD");

        props = new Properties();
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        props.setProperty("mail.smtps.host", System.getenv("SMTP_HOST"));
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", System.getenv("SMTP_PORT"));
        props.setProperty("mail.smtp.socketFactory.port",  System.getenv("SMTP_PORT"));
        props.setProperty("mail.smtps.auth", "true");
        props.put("mail.smtps.quitwait", "false");
    }

    @PostMapping("/send")
    public ResponseEntity send(HttpServletRequest request, @RequestParam String text, @RequestParam String address) throws MessagingException {

        Author sender = dbApi.getCurrentUser(request).getBody();

        if (sender == null)
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);

        Session session = Session.getInstance(props, null);
        final MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(username));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(address, false));
        msg.setSubject("WTLL: Регистрация нового автора");
        String code = generateAndSaveCode();

        String registerLink = "//wtllfrontend.cfapps.io/register";

        String mailText =
                "Вы были приглашены участником <b>%s</b> стать автором на сайте киносообщества <b>Where The Legends Live</b>. "
                        + "Для регистрации перейдите <a href=\"%s\">по данной ссылке</a> либо введите код %s вручную на " +
                        "<a href=\"" + registerLink + "\">странице регистрации</a>.";
        String fullText = mailText + (text.isEmpty() ? "" : "<br><br>Сообщение от приглашающего участника:<br>%s");

        String link = registerLink + "/?code=" + code;

        String resultingText = String.format(fullText, sender.getName(), link, code, text);

        System.out.println(resultingText);

        Multipart mp = new MimeMultipart();
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(resultingText, "text/html; charset=utf-8");
        mp.addBodyPart(htmlPart);

        msg.setContent(mp);
        msg.setSentDate(new Date());
        SMTPTransport t = (SMTPTransport)session.getTransport("smtps");
        t.connect(System.getenv("SMTP_HOST"), username, password);
        try {
            t.sendMessage(msg, msg.getAllRecipients());
        }catch (Exception e){
            t.close();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        t.close();
        return new ResponseEntity(HttpStatus.OK);
    }
}
