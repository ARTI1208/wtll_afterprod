package ru.wtll.back.users;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface UserDao extends UserDataAccess {

    ResponseEntity<String> register(User user, HttpServletRequest request, HttpServletResponse response) throws IOException;

    ResponseEntity<String> login(User user, HttpServletRequest request, HttpServletResponse response) throws IOException;
}
