package ru.wtll.back.users;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;

public interface IAuthorsAccess {
    List<Author> getAllAuthors();

    ResponseEntity<Author> getAuthor(int id);

    ResponseEntity createAuthor(String name, String info, String photo_id,  String socialNetworks) throws IOException;

    ResponseEntity<FileSystemResource> getPhoto(int id) throws IOException;
    ResponseEntity<String> getInfo(int id, HttpServletRequest request, HttpServletResponse response) throws IOException;
    ResponseEntity<String> getNickname(int id) throws IOException;
    ResponseEntity<List<SocialNetworkLink>> getSocialNetworks(int id) throws IOException;

    boolean changeSocialNetworks(int id,List<SocialNetworkLink> socialNetworkLinks);
    boolean changeNickname(int id,String nickName) ;
    boolean changeInfo(int id,String info);
    boolean changePhoto(int id,String photo_id );

    ResponseEntity deleteAccount(HttpServletRequest request,HttpServletResponse response);
    ResponseEntity exitAccount(HttpServletRequest request,HttpServletResponse response);
    ResponseEntity addPhoto( MultipartFile file,HttpServletRequest request);


}
