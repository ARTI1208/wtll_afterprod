package ru.wtll.back

import java.io.File

object ServerCommon {
    val dataDirectory = File(System.getProperty("user.home"), "wtll_data")
}