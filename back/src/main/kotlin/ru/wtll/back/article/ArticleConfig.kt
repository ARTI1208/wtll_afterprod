package ru.wtll.back.article

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.logging.log4j.LogManager
import ru.wtll.back.article.Article.Companion.articlesDirectory
import ru.wtll.back.article.Article.Companion.infoFileFileName
import ru.wtll.back.article.writing.ArticleWritingAPI
import java.io.*
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class ArticleConfig(val name: String, val preview: String) {

    private val logger = LogManager.getLogger(ArticleConfig::class.java)

    var code: String = name.hashCode().toString()

    // PUBLISHED may be first state of the article or come after DRAFT
    enum class State {
        DRAFT,
        PUBLIC;

        @JsonValue
        override fun toString(): String {
            return name.toLowerCase()
        }
    }

    enum class Type {
        REVIEW,
        COMPILATION,
        LONGREAD,
        PODCAST;

        companion object {
            fun isValid(sourceType: String, targetType: Type): Boolean {
                return (sourceType == "all" || sourceType == targetType.toString())
            }
        }

        fun isValid(sourceType: String): Boolean {
            return (sourceType == "all" || sourceType == toString())
        }

        @JsonValue
        override fun toString(): String {
            return name.toLowerCase()
        }
    }

    companion object {
        val defaultDate = Date(0)
    }

    val articleFolder: File
        @JsonIgnore
        get() {
            return File(articlesDirectory,
                    name.hashCode().toString()
                            + File.separator
                            + state.toString())
        }

    val componentsFolder: File
        @JsonIgnore
        get() {
            return File(articleFolder, "components")
        }

    private val infoFile: File
        @JsonIgnore
        get() {
            return File(articleFolder, infoFileFileName)
        }

    var state: State = State.DRAFT

    var type: Type = Type.REVIEW

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    var creationDate: Date = defaultDate

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    var lastModifiedDate: Date = defaultDate

    var tags = listOf<String>()

    var authorId = 0

    private fun getArticleDirectoryOnDisk(): File? {
        val localName = articlesDirectory.list()?.find {
            it == name.hashCode().toString()
        }
        return if (localName == null)
            null
        else
            File(articlesDirectory, localName)
    }

    private fun getArticleOnDiskInfo(state: State): File? {
        val localDir = getArticleDirectoryOnDisk()
        return if (localDir == null)
            null
        else
            File(localDir,
                    state.toString()
                            + File.separator
                            + infoFileFileName
            )
    }

    fun write() {
        val folderToWrite = articleFolder
        if (!folderToWrite.exists()) {
            folderToWrite.mkdirs()
        }

        writeToStream(FileOutputStream(infoFile), getArticleOnDiskInfo(state).let {
            return@let if (it == null) {
                null
            } else {
                FileInputStream(it)
            }
        })
    }

    fun writeToStream(out: OutputStream, input: InputStream?) {
        try {
            val fromFile = jacksonObjectMapper().readValue(input, this.javaClass)
            creationDate = fromFile.creationDate
        } catch (e: Exception) {
            logger.error("Cannot read previous version of article: $this, error = ${e.message}")
        }

        if (creationDate == defaultDate) {
            creationDate = Date()
            lastModifiedDate = Date(creationDate.time)
        } else {
            logger.info("Found previous version of article, do not update creationDate")
            lastModifiedDate = Date()
        }

        jacksonObjectMapper().writeValue(out, this)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArticleConfig

        if (name != other.name) return false
        if (code != other.code) return false
        if (state != other.state) return false
        if (type != other.type) return false
        if (creationDate != other.creationDate) return false
        if (lastModifiedDate != other.lastModifiedDate) return false
        if (tags != other.tags) return false
        if (preview != other.preview) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + code.hashCode()
        result = 31 * result + state.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + creationDate.hashCode()
        result = 31 * result + lastModifiedDate.hashCode()
        result = 31 * result + tags.hashCode()
        result = 31 * result + preview.hashCode()
        return result
    }

}