package ru.wtll.back.article.components

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.File

interface ComponentContainer: ArticleComponent {

    override val isContainer: Boolean
        get() = true
}