package ru.wtll.back.article

import ru.wtll.back.ServerCommon
import java.io.File
import java.io.FileReader
import java.io.FileWriter

class MainManager {

    companion object {

        fun getMainFile(directory: File = ServerCommon.dataDirectory): File {
            return File(directory, "main")
        }

        var lastResult: MutableMap<ArticleConfig.Type, String> = getMainArticles()
        var lastModified: Long = getMainFile().lastModified()

        fun getMainArticles(directory: File = ServerCommon.dataDirectory): MutableMap<ArticleConfig.Type, String> {
            val mainFile = getMainFile(directory)
            if (!mainFile.exists()) {
                return mutableMapOf()
            }

            if (lastModified == mainFile.lastModified()) {
                return lastResult;
            }

            val map = HashMap<ArticleConfig.Type, String>()

            FileReader(mainFile).useLines { lines ->
                lines.forEach { line ->
                    val list = line.split("=")
                    if (list.size == 2) {
                        val key = list[0]
                        val value = list[1]
                        val type = ArticleConfig.Type.values().find { it.toString() == key }
                        if (type != null) {
                            map[type] = value
                        }
                    }
                }
            }

            lastResult = map
            lastModified = mainFile.lastModified()

            return map
        }

        fun getMainArticleOfType(type: String): String? {
            return getMainArticles().entries.find { it.key.toString() == type }?.value
        }

        fun getMainArticleOfType(type: ArticleConfig.Type): String? {
            return getMainArticles().getOrDefault(type, null)
        }

        fun setMainArticle(article: Article, directory: File = ServerCommon.dataDirectory) {
            val mainFile = getMainFile(directory)
            val map = getMainArticles()
            map[article.config.type] = article.config.code

            val builder = StringBuilder()
            map.entries.forEach {
                builder
                        .append(it.key.toString())
                        .append('=')
                        .appendln(it.value)
            }

            FileWriter(mainFile).use { writer ->
                writer.write(builder.toString())
            }
        }
    }
}