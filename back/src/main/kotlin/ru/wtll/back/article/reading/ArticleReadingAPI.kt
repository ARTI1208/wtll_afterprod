package ru.wtll.back.article.reading

import org.apache.logging.log4j.LogManager
import org.apache.tika.Tika
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.wtll.back.article.Article
import ru.wtll.back.article.ArticleConfig
import ru.wtll.back.article.MainManager
import ru.wtll.back.users.PostgresDAO
import java.io.File

@RestController
@RequestMapping("api/article")
class ArticleReadingAPI(val directory: File = Article.articlesDirectory) {

    val fileTypeDetector = Tika()

    private val logger = LogManager.getLogger(ArticleReadingAPI::class.java)

    @GetMapping("/read")
    @ResponseBody
    fun getArticle(@RequestParam id: String,
                   @RequestParam(defaultValue = "public") visibility: String,
                   @RequestParam(defaultValue = "false") withText: Boolean
    ): ResponseEntity<Article> {

        logger.info("Calling read")

        val result =
                Article.read(id, visibility, withText, directory)
                        ?: return ResponseEntity(HttpStatus.NOT_FOUND)

        return ResponseEntity(result, HttpStatus.OK)
    }

    @GetMapping("/last")
    @ResponseBody
    fun getLastArticles(@RequestParam(defaultValue = "5") count: Int,
                        @RequestParam(defaultValue = "all") type: String,
                        @RequestParam(defaultValue = "public") visibility: String,
                        @RequestParam(defaultValue = "[]") except: Array<String>
    ): ResponseEntity<List<Article>> {

        logger.info("Calling last")

        if (count <= 0) {
            logger.warn("Invalid count in last: $count")
            return ResponseEntity(emptyList(), HttpStatus.OK)
        }

        // TODO more effective algorithm?
        val result = Article.articlesDirectory
                .listFiles()
                ?.mapNotNull { Article.read(it, visibility) }
                ?.filter {
                    ArticleConfig.Type.isValid(type, it.config.type)
                            && it.config.code !in except && !it.main
                }?.sortedByDescending { it.config.lastModifiedDate }
                ?.take(count)
                ?: return ResponseEntity(emptyList(), HttpStatus.OK)

        return ResponseEntity(result, HttpStatus.OK)
    }

    @GetMapping("/file")
    @ResponseBody
    fun getFile(@RequestParam(name = "article") articleCode: String,
                @RequestParam(name = "code") fileCode: String
    ): ResponseEntity<FileSystemResource> {

        val file = directory
                .toPath()
                .resolve(articleCode)
                .resolve("files")
                .resolve(fileCode)

        val (superType, subtype) = fileTypeDetector.detect(file).split("/")

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType(superType, subtype))
                .body(FileSystemResource(file))
    }

    @GetMapping("/search")
    @ResponseBody
    fun searchArticles(@RequestParam(defaultValue = "") text: String?,
                       @RequestParam(defaultValue = "5") count: Int,
                       @RequestParam(defaultValue = "false") withText: Boolean
    ): ResponseEntity<List<Article>> {

        if (count <= 0 || text.isNullOrEmpty()) {
            logger.warn("Invalid param in search: count = $count, text = $text")
            return ResponseEntity(emptyList(), HttpStatus.OK)
        }

        val realText = text.toLowerCase()

        // TODO more effective algorithm?
        val result = Article.articlesDirectory
                .listFiles()
                ?.mapNotNull { Article.read(it, withComponents = withText) }
                ?.filter { article ->
                    if (realText.startsWith("#")) {
                        article.config.tags.any { it.toLowerCase().contains(realText.substring(1)) }
                    } else {
                        article.config.name.toLowerCase().contains(realText)
                                || article.config.tags.any { it.toLowerCase().contains(realText) }
                                || (withText && article.containsText(text))
                    }
                }?.sortedByDescending { it.config.lastModifiedDate }
                ?.take(count)
                ?: return ResponseEntity(emptyList(), HttpStatus.OK)

        return ResponseEntity(result, HttpStatus.OK)
    }

    @GetMapping("/main")
    @ResponseBody
    fun getMainArticle(@RequestParam(defaultValue = "review") type: String):
            ResponseEntity<Article> {

        val code = MainManager.getMainArticleOfType(type) ?: return ResponseEntity.notFound().build()
        val result = Article.read(File(Article.articlesDirectory, code)) ?: return ResponseEntity.notFound().build()

        return ResponseEntity(result, HttpStatus.OK)
    }
}