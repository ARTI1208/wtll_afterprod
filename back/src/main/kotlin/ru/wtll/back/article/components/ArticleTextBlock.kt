package ru.wtll.back.article.components

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.io.FileWriter

class ArticleTextBlock(val text: String, val config: TextConfig) : ArticleComponent {

    data class TextConfig(val font: String = "roboto",
                          val fontSizePt: Int = 12,
                          val blockType: BlockType = BlockType.SIMPLE) {

        enum class BlockType {
            SIMPLE,
            TITLE,
            SUBTITLE,
            LEAD,
            SIDEBAR;

            @JsonValue
            override fun toString(): String {
                return name.toLowerCase()
            }
        }

    }

    override fun writeData(componentFolder: File): Boolean {
        if (!componentFolder.isDirectory) {
            componentFolder.delete()
        }

        if (!componentFolder.exists()) {
            componentFolder.mkdir()
        }

        FileWriter(File(componentFolder, "data")).use {
            it.write(asPlainText())
        }

        ObjectMapper().writeValue(File(componentFolder, "config"), config)

        ObjectMapper().writeValue(File(componentFolder, "full"), this)

        return true
    }

    override fun asPlainText(): String {
        return text
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArticleTextBlock

        if (text != other.text) return false
        if (config != other.config) return false

        return true
    }

    override fun hashCode(): Int {
        var result = text.hashCode()
        result = 31 * result + config.hashCode()
        return result
    }

    override val isContainer: Boolean
        get() = false


}