package ru.wtll.back.article

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.io.File
import java.io.FileOutputStream

internal class ArticleConfigTest {

    @Test
    fun writeToStream() {
        val config = ArticleConfig("test", "preview").apply {
            type = ArticleConfig.Type.PODCAST
            state = ArticleConfig.State.DRAFT
            tags = listOf("tag1", "tag2")
        }

        val writeTo = File(TestCommon.testsFolder, "config_writing.json")

        config.writeToStream(FileOutputStream(writeTo), null)

        val fromFile = jacksonObjectMapper().readValue(writeTo, ArticleConfig::class.java)

        assertEquals(config, fromFile)

        config.type = ArticleConfig.Type.LONGREAD
        assertNotEquals(config, fromFile)
    }
}